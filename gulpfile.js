const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

require('./elixir-extensions');

require('./laravel-elixir-react');

// var gulp = require('gulp');
// var exec = require('child_process').exec;

// gulp.task('ping', function(cb) {
//     exec('ping localhost', function (err, stdout, stderr) {
//         console.log(stdout);
//         console.log(stderr);
//         cb(err);
//     });
// });

// var Task = elixir.Task;
//
// elixir.extend('ping', function(host) {
//     new Task('ping', function() {
//         exec('ping ' + host, function (err, stdout, stderr) {
//             console.log(stdout);
//             console.log(stderr);
//         });
//     });
// });

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix.sass('app.scss')
       .webpack('app.js')
    .browserSync({proxy: 'http://elixir.dev'})
    .ping('google.com.au')
    .react('main.js');
});
