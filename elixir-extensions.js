var Elixir = require('laravel-elixir');
var gulp = require('gulp');
var exec = require('child_process').exec;

// gulp.task('ping', function(cb) {
//     exec('ping localhost', function (err, stdout, stderr) {
//         console.log(stdout);
//         console.log(stderr);
//         cb(err);
//     });
// });

var Task = Elixir.Task;

Elixir.extend('ping', function(host) {
    new Task('ping', function() {
        exec('ping ' + host, function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
        });
    });
});