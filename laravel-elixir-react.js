var gulp = require('gulp');

var Elixir = require('laravel-elixir');
var gulpWebpack = require('webpack-stream');

var $ = Elixir.Plugins;
var config = Elixir.config;

Elixir.extend('react', function (src, output, baseDir, options) { //mix.react
    var paths = prepGulpPaths(src, output, baseDir);
    // console.log(paths);

    return new Elixir.Task('react', function() {
        this.log(paths.src.path, paths.output.path);

        // console.log('React is running..');
        return gulp.src(paths.src.path)
            .pipe(gulpWebpack(options || {}, require('webpack')))
            .on('error', function (e) {
                new Elixir.Notification('React compilation failed!');

                this.emit('end');
            })
            .pipe($.rename(paths.output.name))
            .pipe($.if(config.production, $.uglify(config.js.uglify.options)))
            .pipe(gulp.dest(paths.output.baseDir))
            .pipe(new Elixir.Notification('Webpack compiled!'));
    });    //gulp react
});

function prepGulpPaths(src, output, baseDir) {
    return new Elixir.GulpPaths()
        .src(src, baseDir || config.get('assets.js.folder'))
        .output(output || config.get('public.js.outputFolder'), 'bundle.js');
}